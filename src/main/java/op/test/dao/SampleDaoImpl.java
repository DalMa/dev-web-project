package op.test.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;


@Repository
public class SampleDaoImpl implements SampleDao {
	private static final String NAMESPACE = SampleDaoImpl.class.getPackage().getName() + ".SampleDao.";
	
	private static Logger LOGGER = (Logger) LoggerFactory.getLogger(SampleDaoImpl.class);
	
    @Autowired
    @Qualifier("sqlSessionTemplate")
    private SqlSessionTemplate sqlSessionTemplate;
    

}
