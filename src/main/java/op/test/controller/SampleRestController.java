package op.test.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import op.test.service.SampleService;

@RequestMapping("/rest")
@Controller
public class SampleRestController {
	private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(SampleRestController.class);
	
	@Autowired 
	SampleService sampleService;
	
}
