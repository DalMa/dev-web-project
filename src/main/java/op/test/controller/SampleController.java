package op.test.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import op.test.service.SampleService;

@Controller
public class SampleController {
	private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(SampleController.class);
	
	@Autowired 
	private SampleService sampleService;
	
	/**
	 * 메인화면 메서드
	 * @return String
	 * 
	 * */
	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public String testMain(Model model) {
		return "test";
	}
	/**
	 * 메인화면 메서드
	 * @return String
	 * 
	 * */
	@RequestMapping(value = {"/test"}, method = RequestMethod.GET)
	public String testGrid(Model model) {
		return "test-grid";
	}
	
}
