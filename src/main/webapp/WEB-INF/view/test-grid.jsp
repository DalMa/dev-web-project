<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- 부트스트랩은 모바일 우선이므로 모바일에서 적절한 렌더링이 되도록 아래 태그를 추가해야 함 http://bootstrapk.com/css/ -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>ALPHA TEAM</title>

    <!-- 부트스트랩 -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
    <link href="css/starter-template.css" rel="stylesheet">
  </head>
  <body>

    
    <div class="container">
    
      <div class="starter-template">
        <h1>THIS IS SPARTA!!</h1>
        <p class="lead">I'm your father. Th~ Th~</p>
        
        <h2 class="sub-header">Section title</h2>
		<div id="simpleGrid" class="table-responsive"></div>
		<input type="button" value="prepend" id="prepend"/>
	    <input type="button" value="append" id="append"/>
	    <input type="button" value="clear" id="clear"/>
      </div>
      
    </div><!-- /.container -->



    <!-- jQuery (부트스트랩의 자바스크립트 플러그인을 위해 필요합니다) -->
    <script src="js/lib/jquery/jquery-2.1.1.min.js"></script>
    <!-- 모든 컴파일된 플러그인을 포함합니다 (아래), 원하지 않는다면 필요한 각각의 파일을 포함하세요 -->
    <script src="js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="js/lib/fe.grid/code-snippet.min.js"></script>
	<script src="js/lib/fe.grid/simple-grid.min.js"></script>
	<script src="js/lib/fe.grid/dummy.js"></script>
	
	
	  <script>
	  var simpleGrid = new ne.component.SimpleGrid({
		    $el: $('#simpleGrid'),
		    className: {
		        table: 'table table-striped'
		    },
		    rowHeight: 25,    //line 당 pixel
		    displayCount: 20,  //영역에 보여줄 line 갯수
		    headerHeight: 30,
		    scrollX: false,
		    scrollY: true,
		    keyEventBubble: false,  //key 입력시 이벤트 버블링 할지 여부
		    defaultColumnWidth: 50,
		    border: 0,
		    opacity: '0.5',
		    /*
		    //keyColumnName: 'column1',
		    color: {
		        border: 'red',
		        th: 'yellow',
		        td: '#FFFFFF',
		        selection: 'blue'
		    },
		    className: {
		        table: 'table_class',
		        tr: 'tr_class',
		        td: 'td_class'
		    },
	  */
		    columnModelList: [
		        {
		            columnName: 'column1',
		            title: '컬럼1',
		            width: 70,
		            align: 'center',
		            formatter: function(value, rowData) {
		                return '<input type="button" class="test_click" value="' + value + '"/>';
		            }
		        },
		        {
		            columnName: 'column2',
		            title: '컬럼2',
		            width: 60
		        },
		        {
		            columnName: 'column3',
		            title: '컬럼3',
		            width: 70
		        },
		        {
		            columnName: 'column4',
		            title: '컬럼4',
		            width: 80
		        },
		        {
		            columnName: 'column5',
		            title: '컬럼5',
		            width: 90
		        }
		    ]
		}
	  );
	  
	    //데이터 insert
	    simpleGrid.setList(dummyDataList);

	    //버튼 누를경우 삭제되도록 처리
	    simpleGrid.on('click', function(customEvent) {
	        if (customEvent.$target.hasClass('removeBtn')) {
	            alert('삭제 - ' + customEvent.$target.val());
	            simpleGrid.remove(customEvent.rowKey);
	        }
	    });

	    $('#append').on('click', function() {
	        simpleGrid.append([{
	            'column1': 'append',
	            'column2': 'append',
	            'column3': 'append',
	            'column4': 'append',
	            'column5': 'append'
	        },{
	            'column1': 'append',
	            'column2': 'append',
	            'column3': 'append',
	            'column4': 'append',
	            'column5': 'append'
	        }]);
	    });

	    $('#prepend').on('click', function() {
	        simpleGrid.prepend([{
	            'column1': 'prepend',
	            'column2': 'prepend',
	            'column3': 'prepend',
	            'column4': 'prepend',
	            'column5': 'prepend'
	        },{
	            'column1': 'prepend',
	            'column2': 'prepend',
	            'column3': 'prepend',
	            'column4': 'prepend',
	            'column5': 'prepend'
	        }]);
	    });

	    $('#clear').on('click', function() {
	        simpleGrid.clear();
	    });
  </script>
  </body>
</html>